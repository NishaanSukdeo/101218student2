package com.safebear.auto.syntax;

import org.testng.annotations.Test;

public class EmployeeTest {
    @Test
    public void testEmployee() {

        //This is where we create our objects
        Employee hannah = new Employee();
        Employee bob = new Employee();
        SalesEmployee victoria = new SalesEmployee();

        //This is where we employ regular employees
        hannah.employ();
        bob.fire();

        //this is sales employess only
        victoria.employ();
        victoria.changeCar("mazda");


        //Lets print Employees state to screen
        System.out.println("Hannah employment state: " + hannah.isEmployed());
        System.out.println("Bobs employment state:" + bob.isEmployed());
        System.out.println("Victoria employment state: " + victoria.isEmployed());
        System.out.println("Victoria's car is a " + victoria.car);
        System.out.println("Number of employees is: ");

    }
}