package com.safebear.auto.pages;

import com.safebear.auto.pages.locators.ToolsPageLocators;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.openqa.selenium.WebDriver;

@RequiredArgsConstructor
public class ToolsPage {
    ToolsPageLocators toolsPageLocators = new ToolsPageLocators();

    @NonNull
    WebDriver driver;

    public String getPageTitle() {
        return driver.getTitle();
    }

    public String isSuccessfulMessage() {
        return driver.findElement(toolsPageLocators.getSuccessfulLoginMessage()).getText();
    }
}
