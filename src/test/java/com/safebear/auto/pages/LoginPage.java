package com.safebear.auto.pages;

import com.safebear.auto.pages.locators.LoginPageLocators;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.openqa.selenium.WebDriver;

@RequiredArgsConstructor
public class LoginPage {
    LoginPageLocators loginPageLocators = new LoginPageLocators();

    @NonNull
    WebDriver driver;

    public String getPageTitle() {
        return driver.getTitle();
    }

    //Use this method to keep all login info together
 /*   public void login(String un, String pw){
        driver.findElement(loginPageLocators.getUsernameLocator()).sendKeys(un);
        driver.findElement(loginPageLocators.getPasswordLocator()).sendKeys(pw);
        driver.findElement(loginPageLocators.getLoginButtonLocator()).click();

    }
    */

    public void enterUsername(String un) {
        driver.findElement(loginPageLocators.getUsernameLocator()).sendKeys(un);
    }

    public void enterPassword(String pw){
        driver.findElement(loginPageLocators.getPasswordLocator()).sendKeys(pw);
    }
    public void clickSubmit() {
        driver.findElement(loginPageLocators.getLoginButtonLocator()).click();
    }

    public String failedMessage(){
        return driver.findElement(loginPageLocators.getFailedLoginMessage()).getText();
    }

    public void rememberMe() {
        driver.findElement(loginPageLocators.getRememberMeCheckbox()).isSelected();
    }


}
