package com.safebear.auto.utils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerOptions;

public class Utils {
    private static final String URL = System.getProperty("url", "http://toolslist.safebear.co.uk:8080");
    private static final String BROWSER = System.getProperty("browser","chrome");

    public static WebDriver getDriver(){
        System.setProperty("webdriver.chrome.driver", "src\\test\\resources\\drivers\\chromedriver.exe");
        ChromeOptions options = new ChromeOptions();
        options.addArguments("window-size=1366,768");


        switch (BROWSER) {

            case "chrome":
                return new ChromeDriver(options);

            case "firefox":
                System.setProperty("webdriver.gecko.driver", "src\\test\\resources\\drivers\\geckodriver.exe");
                FirefoxOptions fireOptions = new FirefoxOptions();
                fireOptions.addArguments("window-size=1366,768");
                return new FirefoxDriver(fireOptions);

            case "ie":
                System.setProperty("webdriver.ie.driver","src\\test\\resources\\drivers\\IEDriverServer.exe");
                InternetExplorerOptions ieOptions = new InternetExplorerOptions();
               // ieOptions.setCapability("window-size=1366,768");
                return new InternetExplorerDriver(ieOptions);

            case "chromeheadless":
                ChromeOptions chromeHeadless = new ChromeOptions();
                chromeHeadless.addArguments("headless","disable-gpu"); //"disable-gpu" is disabling the hardware in headless mode
                return new ChromeDriver(chromeHeadless);

            default:
                return new ChromeDriver(options);
        }
    }

    public static String getUrl() {
        return URL;
    }
}